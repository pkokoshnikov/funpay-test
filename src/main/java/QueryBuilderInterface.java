public interface QueryBuilderInterface {
    String buildQuery(String query, Object... args) ;
}
