import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class QueryBuilder implements QueryBuilderInterface {

    public static final String NULL_VALUE = "NULL";

    @Override
    public String buildQuery(String template, Object... params) {
        StringBuilder sb = new StringBuilder();
        int paramIndex = -1;

        int templateIndex = 0;
        if (template.charAt(template.length() - 1) == '?') {
            template =
                    template + Character.MIN_VALUE; //it's   symbol to handle corner case when ? in the end of template
        }
        while (templateIndex < template.length()) {
            char c = template.charAt(templateIndex);
            if (c == '?') {
                paramIndex = nextParamsIndex(params, paramIndex);
                Object param = params[paramIndex];
                templateIndex = nextTemplateIndex(template, templateIndex);
                char nc = template.charAt(templateIndex);
                if (nc == 'd') {
                    sb.append(formatInt(param));
                } else if (nc == 'f') {
                    sb.append(formatFloat(param));
                } else if (nc == 'a') {
                    if (param instanceof List<?> paramList) {
                        sb.append(paramList.stream().map(QueryBuilder::formatObject).collect(Collectors.joining(", ")));
                    }
                    if (param instanceof Map<?, ?> paramMap) {
                        sb.append(paramMap.entrySet().stream()
                                .map(e -> formatStringWithNameEscape(e.getKey()) + " = " + formatObject(e.getValue()))
                                .collect(Collectors.joining(", ")));
                    }
                } else if (nc == '#') {
                    if (param instanceof List<?> paramList) {
                        sb.append(paramList.stream().map(QueryBuilder::formatStringWithNameEscape)
                                .collect(Collectors.joining(", ")));
                    } else {
                        sb.append(formatStringWithNameEscape(param.toString()));
                    }
                } else if (nc == 's') {
                    sb.append(formatString(param.toString()));
                } else if (nc == ' ' || nc == ';' || nc == '\t' || nc == '\n') {
                    sb.append(formatObject(param.toString())).append(nc);
                } else if (nc == Character.MIN_VALUE) {
                    sb.append(formatObject(param.toString()));
                } else {
                    throw new IllegalArgumentException(
                            "Incorrect template" + template +
                                    " after ? should be 'd','f','#','a','s',';' or whitespace symbol");
                }
            } else if (c == '{') {
                paramIndex = nextParamsIndex(params, paramIndex);
                Object param = params[paramIndex];
                if (param instanceof OptionalBlock optionalBlockParam) {
                    int startBlock = templateIndex;
                    while (c != '}') {
                        templateIndex = nextTemplateIndex(template, templateIndex);
                        c = template.charAt(templateIndex);
                    }
                    int endBlock = templateIndex;
                    if (optionalBlockParam.test()) {
                        sb.append(buildQuery(template.substring(startBlock + 1, endBlock),
                                optionalBlockParam.args()));
                    }
                } else {
                    throw newIllegalArgumentException(param, OptionalBlock.class);
                }
            } else {
                sb.append(c);
            }
            templateIndex = nextTemplateIndex(template, templateIndex);
        }

        return sb.toString();
    }

    private static IllegalArgumentException newIllegalArgumentException(Object param, Class... types) {
        var typesString = Arrays.stream(types).map(Class::getName).collect(Collectors.joining(", "));
        return new IllegalArgumentException(
                "Incorrect type of param: " + param + ", type was:" + param.getClass() + ", but it should be:" +
                        typesString);
    }

    private static String formatInt(Object value) {
        return switch (value) {
            case null -> NULL_VALUE;
            case Number numberValue -> String.valueOf(numberValue.longValue());
            case Boolean booleanValue -> booleanValue ? "1" : "0";
            default -> throw newIllegalArgumentException(value, Number.class);
        };
    }

    private static String formatFloat(Object value) {
        return switch (value) {
            case null -> NULL_VALUE;
            case Number numberValue -> String.valueOf(numberValue.doubleValue());
            case Boolean booleanValue -> booleanValue ? "1.0" : "0.0";
            default -> throw newIllegalArgumentException(value, Number.class, Boolean.class);
        };
    }

    private static String formatString(Object value) {
        return switch (value) {
            case null -> NULL_VALUE;
            case String stringValue -> "'" + stringValue.replace("'", "''") + "'";
            default -> throw newIllegalArgumentException(value, String.class);
        };
    }

    private static String formatStringWithNameEscape(Object value) {
        return switch (value) {
            case null -> NULL_VALUE;
            case String stringValue -> "`" + stringValue.replace("'", "''") + "`";
            default -> throw newIllegalArgumentException(value, String.class);
        };
    }

    private static String formatObject(Object value) {
        if (value == null) {
            return NULL_VALUE;
        } else if (value instanceof Float || value instanceof Double) {
            return formatFloat(value);
        } else if (value instanceof Number || value instanceof Boolean) {
            return formatInt(value);
        } else if (value instanceof String) {
            return formatString(value);
        } else {
            throw newIllegalArgumentException(value, Number.class, Boolean.class, String.class);
        }
    }

    private static int nextTemplateIndex(String template, int i) {
        i++;
        if (i > template.length()) {
            throw new IllegalArgumentException("Query build failed, incorrect template:" + template);
        }
        return i;
    }

    private static int nextParamsIndex(Object[] params, int i) {
        i++;
        if (i > params.length) {
            throw new IllegalArgumentException("Query build failed, not enough params");
        }
        return i;
    }
}

