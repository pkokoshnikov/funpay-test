import java.util.function.Supplier;

public record OptionalBlock(Supplier<Boolean> predicate, Object... args) {

    public boolean test() {
        return predicate.get();
    }

    public static OptionalBlock of(Supplier<Boolean> predicate, Object... args) {
        return new OptionalBlock(predicate, args);
    }
}
