import java.util.HashMap;
import java.util.Map;

public class Utils {
    /**
     * hash map with nullable values initializer
     */
    public static Map<String, String> newHashMap(Object... args) {
        if (args.length % 2 == 1) {
            throw new IllegalArgumentException("Size should be even");
        }
        var map = new HashMap<String, String>(args.length / 2);
        for (int i = 0; i < args.length; i += 2) {
            if (args[i] == null) {
                throw new IllegalArgumentException("Key cannot be null");
            }
            map.put(args[i].toString(), args[i + 1] != null ? args[i + 1].toString() : null);
        }

        return map;
    }
}