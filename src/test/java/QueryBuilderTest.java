import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class QueryBuilderTest {
    private QueryBuilderInterface db;

    @BeforeEach
    void setUp() {
        db = new QueryBuilder();
    }

    @Test
    void testQueryBuilder1() {
        var query = db.buildQuery("SELECT name FROM users WHERE user_id = 1");
        assertEquals("SELECT name FROM users WHERE user_id = 1", query);
    }

    @Test
    void testQueryBuilder2() {
        var query = db.buildQuery("SELECT * FROM users WHERE name = ? AND block = 0", "Jack");
        assertEquals("SELECT * FROM users WHERE name = 'Jack' AND block = 0", query);
    }

    @Test
    void testQueryBuilder2_1() {
        var query = db.buildQuery("SELECT * FROM users WHERE block = 0 AND name = ?", "Jack");
        assertEquals("SELECT * FROM users WHERE block = 0 AND name = 'Jack'", query);
    }

    @Test
    void testQueryBuilder2_2() {
        var query = db.buildQuery("SELECT * FROM users WHERE block = 0 AND name = ?;", "Jack");
        assertEquals("SELECT * FROM users WHERE block = 0 AND name = 'Jack';", query);
    }

    @Test
    void testQueryBuilder2_3() {
        var query = db.buildQuery("SELECT * FROM users WHERE block = 0 AND name = ?\n", "Jack");
        assertEquals("SELECT * FROM users WHERE block = 0 AND name = 'Jack'\n", query);
    }

    @Test
    void testQueryBuilder3() {
        var query = db.buildQuery("SELECT ?# FROM users WHERE user_id = ?d AND block = ?d",
                Arrays.asList("name", "email"), 2, true);
        assertEquals("SELECT `name`, `email` FROM users WHERE user_id = 2 AND block = 1", query);
    }

    @Test
    void testQueryBuilder4() {
        var query = db.buildQuery("UPDATE users SET `name` = 'Jack', `email` = NULL WHERE user_id = -1",
                Utils.newHashMap("name", "Jack", "email", null));
        assertEquals("UPDATE users SET `name` = 'Jack', `email` = NULL WHERE user_id = -1", query);
    }

    @Test
    void testQueryBuilder5() {
        List<String> actual = new ArrayList<>();
        for (var value : Arrays.asList(null, true)) {
            actual.add(db.buildQuery("SELECT name FROM users WHERE ?# IN (?a){ AND block = ?d}",
                    "user_id", Arrays.asList(1, 2, 3), OptionalBlock.of(() -> Boolean.TRUE == value, value)));
        }

        List<String> expected = Arrays.asList(
                "SELECT name FROM users WHERE `user_id` IN (1, 2, 3)",
                "SELECT name FROM users WHERE `user_id` IN (1, 2, 3) AND block = 1"
        );
        assertEquals(expected, actual);
    }

}
